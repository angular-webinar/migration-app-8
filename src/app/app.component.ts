import { Component, ViewChild,OnInit, AfterViewInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'demo-app';
  @ViewChild('variable') content: ElementRef;


  ngOnInit(){
    console.log (`Inside the init method :: ${this.content}`)
  }

  ngAfterViewInit(){
    console.log (`Inside the After view init method :: ${this.content.nativeElement}`)
    console.log (this.content.nativeElement.textContent)
  }
}
