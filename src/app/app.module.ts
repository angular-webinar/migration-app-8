import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppChild } from './addChildElement';
import { BaseMenuComponent } from './base-menu/base-menu.component';
import { ServiceOneService } from './service-one.service';

@NgModule({
  declarations: [
    AppComponent,
    AppChild,
    BaseMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [ServiceOneService],
  bootstrap: [AppComponent]
})
export class AppModule { }
