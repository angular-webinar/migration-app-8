import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive(
    {
        selector: '[appChild]'
    }
)
export class AppChild{
    counter = 0;
    @HostListener('click') onclick(){
            const el = __ngRendererCreateElementHelper(this.render, null, 'span');
            el.textContent = 'hello world';
            console.log(this.elementRef.nativeElement)
            this.elementRef.nativeElement
    }
    constructor(private render: Renderer2, private elementRef: ElementRef){

    }
}
type AnyDuringRendererMigration = any;

function __ngRendererSplitNamespaceHelper(name: AnyDuringRendererMigration) {
    if (name[0] === ":") {
        const match = name.match(/^:([^:]+):(.+)$/);
        return [match[1], match[2]];
    }
    return ["", name];
}

function __ngRendererCreateElementHelper(renderer: AnyDuringRendererMigration, parent: AnyDuringRendererMigration, namespaceAndName: AnyDuringRendererMigration) {
    const [namespace, name] = __ngRendererSplitNamespaceHelper(namespaceAndName);
    const node = renderer.createElement(name, namespace);
    if (parent) {
        renderer.appendChild(parent, node);
    }
    return node;
}
